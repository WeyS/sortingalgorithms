# SortingAlgorithms

Write a program that contains 2 tabs "Sort alg" and "Demo".

1. Sort alg - sorting algorithm comparison Bubble, Insert, Quick, Selection, Merge (at least 3).
- TextField for array size;
- TextField for repetition number;
- switch for selecting algorithms;
- Start button;
- Labels with results.

On taping "Start" program should start computing time that takes for each algorithm to sort an array.
Tip:All computations should be performed in background threads (one by one).

On tapping "Start" button you should show activity indicators instead of label with time for sortings you will calculate according to toggles and remove unnecessary label at all.

 

2. Demo - Selection sort algorithm demonstration
- TextField for array size;
- Restart button;
- Next button;
- Table that will show the state of sorting.

Restart button will generate new random array to be sorted.
Next will update table according to current sorting state.
IMPORTANT: You can't use ReloadData() to update UITableView.

Note: For UI element positioning use AutoLayouts.

 UITabBarViewController - Sort alg - UIViewController: Yellow
- UIView: Blue (UILabel, UITextField, UIButton, UISwitch )
- UIStackView: White (UIView (UILabel))
Demo - UIViewController: Yellow
- UIView: Blue (UIButton, UILabel, UITextField)
- UITableView: White (UITableViewCell)

