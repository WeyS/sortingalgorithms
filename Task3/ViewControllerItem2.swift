//
//  ViewControllerItem2.swift
//  Task3
//
//  Created by Maxim on 28.09.2021.
//

import UIKit

class ViewControllerItem2: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var ifSortedText: UILabel!
    
    
    var arr = [Int]()
    var currentState = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func restartButton(_ sender: Any) {
        currentState = 0
        buttonNext.isEnabled = true
        ifSortedText.text = ""
        
        tableView.performBatchUpdates({
            for x in arr.indices{
                tableView.deleteRows(at: [IndexPath(row: x, section: 0)], with: .automatic)
            }

            arr = Array(repeating: 0,count:Int(textField.text!)!).enumerated().compactMap {
                tableView.insertRows(at: [IndexPath(row: $0.offset, section: 0)], with: .automatic)
                return randNum()
            }
        }, completion: nil)

    }
    
    @IBAction func nextButton(_ sender: Any) {
        var smallestNum = currentState
        
        for y in currentState + 1 ..< arr.count {
            if arr[y] < arr[smallestNum] {
                smallestNum = y
        }
    }
        
        if arr[currentState] != arr[smallestNum]{
            arr.swapAt(currentState, smallestNum)
            tableView.performBatchUpdates({
                
                tableView.reloadRows(at: [IndexPath(row: smallestNum, section: 0)], with: .automatic)
                tableView.reloadRows(at: [IndexPath(row: currentState, section: 0)], with: .automatic)
                
            }, completion: nil)
        }
        currentState = currentState + 1
        
        if currentState == arr.count{
            buttonNext.isEnabled = false
            ifSortedText.text = "Array is sorted!"
        }
        
    }
}

// MARK: - Extending a class
extension ViewControllerItem2 : UITableViewDelegate , UITableViewDataSource{
    
    // MARK: - Creating a table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(arr[indexPath.row])"
        return cell
    }
    
    // MARK: - Generating a random number
    func randNum() -> Int{
        let rand = Int.random(in: 0...100)
        return rand
    }
    
}
