//
//  ViewController.swift
//  Task3
//
//  Created by Maxim on 28.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textFieldSize: UITextField!
    @IBOutlet weak var textFieldRepeat: UITextField!
    @IBOutlet weak var bubbleLable: UILabel!
    @IBOutlet weak var bubbleSwitch: UISwitch!
    @IBOutlet weak var selectSwitch: UISwitch!
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var insertionSwitch: UISwitch!
    @IBOutlet weak var insertionLabel: UILabel!
    @IBOutlet weak var bubbleIndicator: UIActivityIndicatorView!
    @IBOutlet weak var selectIndicator: UIActivityIndicatorView!
    @IBOutlet weak var insertionIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Button implementation
    @IBAction func buttonStart(_ sender: Any) {
        
        DispatchQueue.global().async {
            DispatchQueue.main.async { [self] in
                bubbleLable.text = "Bubble :"
                selectLabel.text = "Select :"
                insertionLabel.text = "Insertion :"
                if bubbleSwitch.isOn{
                    isWorkIndicator(isAnimated: true, indicator: bubbleIndicator)
                }
                if selectSwitch.isOn{
                    isWorkIndicator(isAnimated: true, indicator: selectIndicator)
                }
                if insertionSwitch.isOn{
                    isWorkIndicator(isAnimated: true, indicator: insertionIndicator)
                }
            }
        }

        let arr = randArray()
        
        if bubbleSwitch.isOn{
            DispatchQueue.global().async { [self] in
                let start = DispatchTime.now()
                bubble(array: arr)
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let timeInterval = String(format: "%.2f", Double(nanoTime) / 1_000_000_000)
                DispatchQueue.main.async {
                    bubbleLable.text = "Bubble : \(timeInterval)s"
                    isWorkIndicator(isAnimated: false, indicator: bubbleIndicator)
                }

            }
        }
        
        if selectSwitch.isOn{
            DispatchQueue.global().async { [self] in
                let start = DispatchTime.now()
                selectionSort(array: arr)
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let timeInterval = String(format: "%.2f", Double(nanoTime) / 1_000_000_000)
                DispatchQueue.main.async {
                    selectLabel.text = "Select : \(timeInterval)s"
                    isWorkIndicator(isAnimated: false, indicator: selectIndicator)
                }
            }
        }
        if insertionSwitch.isOn{
            DispatchQueue.global().async { [self] in
                let start = DispatchTime.now()
                insertionSort(array: arr)
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let timeInterval = String(format: "%.2f", Double(nanoTime) / 1_000_000_000)
                DispatchQueue.main.async {
                    insertionLabel.text = "Insertion : \(timeInterval)s"
                    isWorkIndicator(isAnimated: false, indicator: insertionIndicator)
                }
            }
        }
        
    }
    
    // MARK: - Indicator Activity
    func isWorkIndicator(isAnimated : Bool , indicator : UIActivityIndicatorView){
        if isAnimated{
            indicator.startAnimating()
            indicator.isHidden = false
        }else{
            indicator.stopAnimating()
            indicator.isHidden = true
        }
    }
    
    // MARK: - Random number implementation
    func randArray() -> [Int]{
        var randArr = [Int]()
        var iter = 0
        repeat{
            if let text = Int(textFieldSize.text!){
                for _ in 0..<text{
                    let rand = Int.random(in: 0...1000)
                    randArr.append(rand)
                }
            }
            iter += 1
        
        }while(iter <= Int(textFieldRepeat.text!)!)
        return randArr
    }
    
    // MARK: - Bubble Sort Algorithm Implementation
    func bubble(array : [Int]){
        var arr = array
        for i in 0..<arr.count {
          for j in 1..<arr.count - i {
            if arr[j] < arr[j-1] {
              let tmp = arr[j-1]
              arr[j-1] = arr[j]
              arr[j] = tmp
            }
          }
        }
    }
    
    // MARK: - Selection Sort Algorithm Implementation
    func selectionSort(array: [Int]) {
      var arr = array
      for x in 0 ..< arr.count - 1 {
        var smallestNum = x
        for y in x + 1 ..< arr.count {
          if arr[y] < arr[smallestNum] {
            smallestNum = y
          }
        }

        if x != smallestNum {
          arr.swapAt(x, smallestNum)
        }
      }
    }
    
    // MARK: - Insertion Sort Algorithm Implementation
    func insertionSort(array: [Int]) {
      var arr = array
      for i in 1..<arr.count {
        var currentIndex = i
        let temp = arr[currentIndex]
        while currentIndex > 0 && temp < arr[currentIndex - 1] {
          arr[currentIndex] = arr[currentIndex - 1]
          currentIndex -= 1
        }
        arr[currentIndex] = temp
      }
    }
    
    
}

